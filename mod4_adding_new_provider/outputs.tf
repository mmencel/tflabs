###
# OUTPUTS
###

output "azurerm_instance_public_ip" {
  value = "${azurerm_public_ip.lbpip.ip_address}"
}

output "azurerm_instance_public_dns" {
  value = "${azurerm_public_ip.lbpip.fqdn}"
}

output "azurerm_dns_cname" {
  value = "${azurerm_dns_cname_record.lb.id}"
}
