###
# PROVIDERS
###

provider "azurerm" {
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
}

###
# DATA
###

###
# RESOURCES
###

resource "azurerm_resource_group" "rgtf" {
  name     = "rgtf"
  location = "${var.location}"

  tags {
    Name        = "${var.environment_tag}-rg"
    BillingCode = "${var.billing_code_tag}"
    Environment = "${var.environment_tag}"
  }
}

resource "azurerm_storage_account" "satf" {
  name                     = "satf"
  resource_group_name      = "${azurerm_resource_group.rgtf.name}"
  location                 = "eastus"
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags {
    Name        = "${var.environment_tag}-sa"
    BillingCode = "${var.billing_code_tag}"
    Environment = "${var.environment_tag}"
  }
}

resource "azurerm_storage_container" "sc-logs" {
  name                  = "logs"
  resource_group_name   = "${azurerm_resource_group.rgtf.name}"
  storage_account_name  = "${azurerm_storage_account.satf.name}"
  container_access_type = "private"
}

resource "azurerm_storage_container" "sc-website" {
  name                  = "website"
  resource_group_name   = "${azurerm_resource_group.rgtf.name}"
  storage_account_name  = "${azurerm_storage_account.satf.name}"
  container_access_type = "private"
}

resource "azurerm_storage_blob" "website" {
  name                   = "index.html"
  resource_group_name    = "${azurerm_resource_group.rgtf.name}"
  storage_account_name   = "${azurerm_storage_account.satf.name}"
  storage_container_name = "${azurerm_storage_container.sc-website.name}"
  type                   = "block"
  source                 = "index.html"
}

resource "azurerm_storage_blob" "graphic" {
  name                   = "Globo_logo_Vert.png"
  resource_group_name    = "${azurerm_resource_group.rgtf.name}"
  storage_account_name   = "${azurerm_storage_account.satf.name}"
  storage_container_name = "${azurerm_storage_container.sc-website.name}"
  type                   = "block"
  source                 = "Globo_logo_Vert.png"
}

resource "azurerm_availability_set" "avset" {
  name                = "avset"
  resource_group_name = "${azurerm_resource_group.rgtf.name}"
  location            = "${var.location}"
  managed             = true

  tags {
    Name        = "${var.environment_tag}-as"
    BillingCode = "${var.billing_code_tag}"
    Environment = "${var.environment_tag}"
  }
}

resource "azurerm_virtual_network" "vnet-tf" {
  name                = "vnet-tf"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.rgtf.name}"
  address_space       = ["${var.address_space}"]

  tags {
    Name        = "${var.environment_tag}-vnet"
    BillingCode = "${var.billing_code_tag}"
    Environment = "${var.environment_tag}"
  }
}

resource "azurerm_subnet" "subnet-tf" {
  count                = "${var.subnet_count}"
  name                 = "subnet${count.index}"
  resource_group_name  = "${azurerm_resource_group.rgtf.name}"
  virtual_network_name = "${azurerm_virtual_network.vnet-tf.name}"
  address_prefix       = "${cidrsubnet(var.address_space,8,count.index+1)}"
}

resource "azurerm_lb" "lb" {
  name                = "rg-lb"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.rgtf.name}"

  frontend_ip_configuration {
    name                 = "LoadBalancerFrontEnd"
    public_ip_address_id = "${azurerm_public_ip.lbpip.id}"
  }

  tags {
    Name        = "${var.environment_tag}-lb"
    BillingCode = "${var.billing_code_tag}"
    Environment = "${var.environment_tag}"
  }
}

resource "azurerm_lb_backend_address_pool" "backend_pool" {
  name                = "BackEndPool1"
  resource_group_name = "${azurerm_resource_group.rgtf.name}"
  loadbalancer_id     = "${azurerm_lb.lb.id}"
}

resource "azurerm_lb_nat_rule" "tcp" {
  name                           = "SSH-Access${count.index}"
  resource_group_name            = "${azurerm_resource_group.rgtf.name}"
  loadbalancer_id                = "${azurerm_lb.lb.id}"
  protocol                       = "tcp"
  frontend_port                  = "1002${count.index + 1}"
  backend_port                   = 22
  frontend_ip_configuration_name = "LoadBalancerFrontEnd"
  count                          = "${var.node_count}"
}

resource "azurerm_lb_rule" "lb_rule" {
  name                           = "LBRule"
  resource_group_name            = "${azurerm_resource_group.rgtf.name}"
  loadbalancer_id                = "${azurerm_lb.lb.id}"
  protocol                       = "tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = "LoadBalancerFrontEnd"
  enable_floating_ip             = false
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.backend_pool.id}"
  idle_timeout_in_minutes        = 5
  probe_id                       = "${azurerm_lb_probe.lb_probe.id}"
  depends_on                     = ["azurerm_lb_probe.lb_probe"]
}

resource "azurerm_lb_probe" "lb_probe" {
  name                = "tcpProbe"
  resource_group_name = "${azurerm_resource_group.rgtf.name}"
  loadbalancer_id     = "${azurerm_lb.lb.id}"
  protocol            = "tcp"
  port                = 80
  interval_in_seconds = 5
  number_of_probes    = "${var.node_count}"
}

resource "azurerm_public_ip" "lbpip" {
  name                         = "lbpip"
  resource_group_name          = "${azurerm_resource_group.rgtf.name}"
  public_ip_address_allocation = "static"
  location                     = "${var.location}"
  domain_name_label            = "tf-lb"

  tags {
    Name        = "${var.environment_tag}-pip"
    BillingCode = "${var.billing_code_tag}"
    Environment = "${var.environment_tag}"
  }
}

resource "azurerm_network_security_group" "nsg" {
  name                = "nsg"
  resource_group_name = "${azurerm_resource_group.rgtf.name}"
  location            = "${var.location}"

  security_rule {
    name                       = "SSH"
    priority                   = "1001"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags {
    Name        = "${var.environment_tag}-nsg"
    BillingCode = "${var.billing_code_tag}"
    Environment = "${var.environment_tag}"
  }
}

resource "azurerm_network_interface" "nic" {
  name                = "nic${count.index}"
  resource_group_name = "${azurerm_resource_group.rgtf.name}"
  location            = "${var.location}"
  count               = "${var.node_count}"

  ip_configuration {
    name                                    = "ipconfig${count.index}"
    subnet_id                               = "${element(azurerm_subnet.subnet-tf.*.id,count.index % var.subnet_count)}"
    private_ip_address_allocation           = "dynamic"
    load_balancer_backend_address_pools_ids = ["${azurerm_lb_backend_address_pool.backend_pool.id}"]
    load_balancer_inbound_nat_rules_ids     = ["${element(azurerm_lb_nat_rule.tcp.*.id, count.index)}"]
  }

  tags {
    Name        = "${var.environment_tag}-nic"
    BillingCode = "${var.billing_code_tag}"
    Environment = "${var.environment_tag}"
  }
}

resource "azurerm_virtual_machine" "vm" {
  name                             = "vm${count.index}"
  resource_group_name              = "${azurerm_resource_group.rgtf.name}"
  availability_set_id              = "${azurerm_availability_set.avset.id}"
  location                         = "${var.location}"
  network_interface_ids            = ["${element(azurerm_network_interface.nic.*.id, count.index)}"]
  vm_size                          = "Standard_A0"
  count                            = "${var.node_count}"
  delete_os_disk_on_termination    = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.4"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk${count.index}"
    os_type           = "linux"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "nginx${count.index}"
    admin_username = "${var.user}"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/${var.user}/.ssh/authorized_keys"
      key_data = "${file("~/.ssh/id_rsa.pub")}"
    }
  }

  provisioner "file" {
    content = <<EOF
/var/.log/nginx/*.log {
  create 0644 nginx nginx
  daily
  rotate 10
  missingok
  compress
  sharedscripts
  postrotate
    azcopy --source /var/log/nginx/ --destination https://${azurerm_storage_account.satf.name}.blob.core.windows.net/${azurerm_storage_container.sc-logs.name}/vm${count.index}/nginx/ --dest-key ${azurerm_storage_account.satf.primary_access_key} --recursive --quiet --exclude-older
  endscript
    }
EOF

    destination = "/home/${var.user}/nginx"

    connection {
      type        = "ssh"
      port        = "1002${count.index + 1}"
      host        = "${azurerm_public_ip.lbpip.ip_address}"
      user        = "${var.user}"
      private_key = "${file("~/.ssh/id_rsa")}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum install epel-release -y",
      "sudo yum install nginx -y",
      "sudo systemctl start nginx",
      "sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc",
      "sudo sh -c 'echo -e \"[packages-microsoft-com-prod]\nname=packages-microsoft-com-prod \nbaseurl= https://packages.microsoft.com/yumrepos/microsoft-rhel7.3-prod\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc\" > /etc/yum.repos.d/dotnetdev.repo'",
      "sudo yum install libunwind libicu -y",
      "sudo yum install dotnet-sdk-2.1.101 -y",
      "wget -O azcopy.tar.gz https://aka.ms/downloadazcopyprlinux",
      "tar -xf azcopy.tar.gz",
      "sudo ./install.sh",
      "sudo cp /home/${var.user}/nginx /etc/logrotate.d/nginx",
      "azcopy --destination ./index.html --source https://${azurerm_storage_account.satf.name}.blob.core.windows.net/${azurerm_storage_container.sc-website.name}/index.html --source-key ${azurerm_storage_account.satf.primary_access_key} --quiet",
      "azcopy --destination ./Globo_logo_Vert.png --source https://${azurerm_storage_account.satf.name}.blob.core.windows.net/${azurerm_storage_container.sc-website.name}/Globo_logo_Vert.png --source-key ${azurerm_storage_account.satf.primary_access_key} --quiet",
      "sudo cp /home/${var.user}/index.html /usr/share/nginx/html/index.html",
      "sudo cp /home/${var.user}/Globo_logo_Vert.png /usr/share/nginx/html/Globo_logo_Vert.png",
      "sudo logrotate -f /etc/logrotate.conf",
    ]

    connection {
      type        = "ssh"
      port        = "1002${count.index + 1}"
      host        = "${azurerm_public_ip.lbpip.ip_address}"
      user        = "${var.user}"
      private_key = "${file("~/.ssh/id_rsa")}"
    }
  }

  tags {
    Name        = "${var.environment_tag}-vm${count.index}"
    BillingCode = "${var.billing_code_tag}"
    Environment = "${var.environment_tag}"
  }
}

# Azure DNS

resource "azurerm_dns_cname_record" "lb" {
  name                = "${var.environment_tag}-website"
  zone_name           = "${var.dns_zone_name}"
  resource_group_name = "${var.dns_resource_group}"
  ttl                 = "30"
  record              = "${azurerm_public_ip.lbpip.fqdn}"

  tags {
    Name        = "${var.environment_tag}-website"
    BillingCode = "${var.billing_code_tag}"
    Environment = "${var.environment_tag}"
  }
}
