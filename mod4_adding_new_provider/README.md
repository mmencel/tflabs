# Module Four

Multiple subnets.  Azure DNS.

* Deploy `var.node_count` nodes behind a load balancer with `var.subnet_count` subnets.
* Create Storage account and container.
* Deploy files to blob storage.
* Run a file and remote-exec provisioner to deploy azcopy and move the files from blob storage so azure can use them.
* Create DNS entries
