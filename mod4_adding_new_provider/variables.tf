variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}

variable "address_space" {
  description = "Address space for VNET"
}

variable "dns_zone_name" {
  description = "DNS Zone Name"
}

variable "dns_resource_group" {
  description = "Azure DNS resource group"
}

variable "lb_ip_dns_name" {
  description = "DNS for Load Balancer IP"
}

variable "location" {
  description = "Azure Region"
}

variable "node_count" {
  description = "Number of backend nodes to provision"
}

variable "subnet_count" {
  description = "Number of subnets to provision"
}

variable "user" {
  description = "SSH Username"
}

variable "billing_code_tag" {}
variable "environment_tag" {}
