variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}

variable "lb_ip_dns_name" {
  description = "DNS for Load Balancer IP"
}

variable "location" {
  description = "Azure Region"
}

variable "node_count" {
  description = "Number of backend nodes to provision"
}

variable "user" {
  description = "SSH Username"
}
