###
# PROVIDERS
###

provider "azurerm" {
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
}

###
# DATA
###

# data "azurerm_platform_image" "centos" {
#   location  = "${var.location}"
#   publisher = "OpenLogic"
#   offer     = "CentOS"
#   sku       = "7.4"
# }

# data "azurerm_image" "image" {
#   name                = "centos-7.4-mmencel"
#   resource_group_name = "images"
# }

###
# RESOURCES
###

resource "azurerm_resource_group" "rgtf" {
  name     = "rgtf"
  location = "${var.location}"
}

resource "azurerm_availability_set" "avset" {
  name                = "avset"
  resource_group_name = "${azurerm_resource_group.rgtf.name}"
  location            = "${var.location}"
  managed             = true
}

resource "azurerm_virtual_network" "vnet-tf" {
  name                = "vnet-tf"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.rgtf.name}"
  address_space       = ["10.1.0.0/24"]
}

resource "azurerm_subnet" "subnet-tf" {
  name                 = "subnet-tf"
  resource_group_name  = "${azurerm_resource_group.rgtf.name}"
  virtual_network_name = "${azurerm_virtual_network.vnet-tf.name}"
  address_prefix       = "10.1.0.0/24"
}

resource "azurerm_lb" "lb" {
  name                = "rg-lb"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.rgtf.name}"

  frontend_ip_configuration {
    name                 = "LoadBalancerFrontEnd"
    public_ip_address_id = "${azurerm_public_ip.lbpip.id}"
  }
}

resource "azurerm_lb_backend_address_pool" "backend_pool" {
  name                = "BackEndPool1"
  resource_group_name = "${azurerm_resource_group.rgtf.name}"
  loadbalancer_id     = "${azurerm_lb.lb.id}"
}

resource "azurerm_lb_nat_rule" "tcp" {
  name                           = "SSH-Access${count.index}"
  resource_group_name            = "${azurerm_resource_group.rgtf.name}"
  loadbalancer_id                = "${azurerm_lb.lb.id}"
  protocol                       = "tcp"
  frontend_port                  = "1002${count.index + 1}"
  backend_port                   = 22
  frontend_ip_configuration_name = "LoadBalancerFrontEnd"
  count                          = "${var.node_count}"
}

resource "azurerm_lb_rule" "lb_rule" {
  name                           = "LBRule"
  resource_group_name            = "${azurerm_resource_group.rgtf.name}"
  loadbalancer_id                = "${azurerm_lb.lb.id}"
  protocol                       = "tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = "LoadBalancerFrontEnd"
  enable_floating_ip             = false
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.backend_pool.id}"
  idle_timeout_in_minutes        = 5
  probe_id                       = "${azurerm_lb_probe.lb_probe.id}"
  depends_on                     = ["azurerm_lb_probe.lb_probe"]
}

resource "azurerm_lb_probe" "lb_probe" {
  name                = "tcpProbe"
  resource_group_name = "${azurerm_resource_group.rgtf.name}"
  loadbalancer_id     = "${azurerm_lb.lb.id}"
  protocol            = "tcp"
  port                = 80
  interval_in_seconds = 5
  number_of_probes    = "${var.node_count}"
}

resource "azurerm_public_ip" "lbpip" {
  name                         = "lbpip"
  resource_group_name          = "${azurerm_resource_group.rgtf.name}"
  public_ip_address_allocation = "static"
  location                     = "${var.location}"
  domain_name_label            = "tf-lb"
}

resource "azurerm_network_security_group" "nsg" {
  name                = "nsg"
  resource_group_name = "${azurerm_resource_group.rgtf.name}"
  location            = "${var.location}"

  security_rule {
    name                       = "SSH"
    priority                   = "1001"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags {
    environment = "Terraform Demo"
  }
}

resource "azurerm_network_interface" "nic" {
  name                = "nic${count.index}"
  resource_group_name = "${azurerm_resource_group.rgtf.name}"
  location            = "${var.location}"
  count               = "${var.node_count}"

  ip_configuration {
    name                                    = "ipconfig${count.index}"
    subnet_id                               = "${azurerm_subnet.subnet-tf.id}"
    private_ip_address_allocation           = "dynamic"
    load_balancer_backend_address_pools_ids = ["${azurerm_lb_backend_address_pool.backend_pool.id}"]
    load_balancer_inbound_nat_rules_ids     = ["${element(azurerm_lb_nat_rule.tcp.*.id, count.index)}"]
  }
}

resource "azurerm_virtual_machine" "vm" {
  name                             = "vm${count.index}"
  resource_group_name              = "${azurerm_resource_group.rgtf.name}"
  availability_set_id              = "${azurerm_availability_set.avset.id}"
  location                         = "${var.location}"
  network_interface_ids            = ["${element(azurerm_network_interface.nic.*.id, count.index)}"]
  vm_size                          = "Standard_A0"
  count                            = "${var.node_count}"
  delete_os_disk_on_termination    = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.4"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk${count.index}"
    os_type           = "linux"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "nginx${count.index}"
    admin_username = "${var.user}"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/${var.user}/.ssh/authorized_keys"
      key_data = "${file("~/.ssh/id_rsa.pub")}"

      # key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC0JreJuwbWS5MgKf57DTJolx1zKp11w0HwQv2vWUKTykE9juGSyOo3Qq5txqDgmT299YO59ZEfGjqp4qYqpoxwVkszxkbozEG7eshhAFLJAdemtuXyB+Z30t4iB2L07n0TrPcGXVjgCfe+gE40l69riEhag9MzyTBwmTQDDUu2UlCbZpcXMNl2EnV0Z3JseOFsOgIxSGuT/blpPDwvZwK9qIw+eFrbLrkiBghwI+SndIncoEE0EaodgLRJWVgve9MvvIiXj9I1WGT+z0w9TOHDvnnwh8bf4R9z2+Mq/4s0JH29tQ4CEiPknWOh+ZXJWRcsjrZHgy9dpt+ggBaKPkwWMaPtiExx1vZMW0/0Pd0hpnsNiPo5WeGDqRyJQa7Oens4sCjgozdpOBjekIoPQ2ZEDlvTyxK54JXtlaPaldpNhcF92ThOP6EhJu39xAIksIGrWHkpCSLwikeADwRgbqEuuSocS1ug//AH3TpMaOhr79CpBtFeSI838ecMH1KKgf2QXX5XBH9sYUdoiYbWO3VHNppvd1eexm085KyeSi7Lfljv5sO9yWlLfNWvL6SuVdqVwqo9YpwmQxd4RVRxnTq7ny5g/HGt1+78mJ0NVXRR4NtLAYbjY2l3QT2B0fD0zobtM6MIESpBJ/1mp2rlopTk4L0I6nBSiC7swbFm9GoKGQ== mmencel@Matts-MacBook-Pro.local"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum install epel-release -y",
      "sudo yum install nginx -y",
      "sudo systemctl start nginx",
    ]

    connection {
      type        = "ssh"
      port        = "1002${count.index + 1}"
      host        = "${azurerm_public_ip.lbpip.ip_address}"
      user        = "${var.user}"
      private_key = "${file("~/.ssh/id_rsa")}"
    }
  }
}
