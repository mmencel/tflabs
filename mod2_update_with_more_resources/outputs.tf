###
# OUTPUTS
###

output "azure_instance_public_ip" {
  value = "${azurerm_public_ip.lbpip.ip_address}"
}

output "azure_instance_public_dns" {
  value = "${azurerm_public_ip.lbpip.fqdn}"
}
