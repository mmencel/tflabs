# Module Three

* Deploy 2 nodes behind a load balancer.
* Create Storage account and  container.
* Deploy files to blob storage.
* Run a file and remote-exec provisioner to deploy azcopy and move the files from blob storage so azure can use them.

```
sudo yum install epel-release -y
sudo yum install nginx -y
sudo systemctl start nginx
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e \"[packages-microsoft-com-prod]\nname=packages-microsoft-com-prod \nbaseurl= https://packages.microsoft.com/yumrepos/microsoft-rhel7.3-prod\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc\" > /etc/yum.repos.d/dotnetdev.repo'
sudo yum install libunwind libicu -y
sudo yum install dotnet-sdk-2.1.101 -y
wget -O azcopy.tar.gz https://aka.ms/downloadazcopyprlinux
tar -xf azcopy.tar.gz
sudo ./install.sh
sudo cp /home/${var.user}/nginx /etc/logrotate.d/nginx
azcopy --destination ./index.html --source https://${azurerm_storage_account.satf.name}.blob.core.windows.net/${azurerm_storage_container.sc-website.name}/index.html --source-key ${azurerm_storage_account.satf.primary_access_key} --quiet
azcopy --destination ./Globo_logo_Vert.png --source https://${azurerm_storage_account.satf.name}.blob.core.windows.net/${azurerm_storage_container.sc-website.name}/Globo_logo_Vert.png --source-key ${azurerm_storage_account.satf.primary_access_key} --quiet
sudo cp /home/${var.user}/index.html /usr/share/nginx/html/index.html
sudo cp /home/${var.user}/Globo_logo_Vert.png /usr/share/nginx/html/Globo_logo_Vert.png
sudo logrotate -f /etc/logrotate.conf
```