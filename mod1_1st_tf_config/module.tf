###
# VARS
###

variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}

variable "location" {}
variable "user" {}

###
# PROVIDERS
###

provider "azurerm" {
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
}

###
# DATA
###

# data "azurerm_platform_image" "centos" {
#   location  = "${var.location}"
#   publisher = "OpenLogic"
#   offer     = "CentOS"
#   sku       = "7.4"
# }

# data "azurerm_platform_image" "ubuntu" {
#   location  = "${var.location}"
#   publisher = "Canonical"
#   offer     = "UbuntuServer"
#   sku       = "16.04-LTS"
# }

# data "azurerm_image" "image" {
#   name                = "centos-7.4-mmencel"
#   resource_group_name = "images"
# }

###
# RESOURCES
###

resource "azurerm_resource_group" "rg-tf" {
  name     = "rg-tf-mod1"
  location = "${var.location}"
}

resource "azurerm_virtual_network" "net-tf" {
  name                = "net-tf"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.rg-tf.name}"
  address_space       = ["10.1.0.0/24"]
}

resource "azurerm_subnet" "subnet-tf" {
  name                 = "subnet-tf"
  resource_group_name  = "${azurerm_resource_group.rg-tf.name}"
  virtual_network_name = "${azurerm_virtual_network.net-tf.name}"
  address_prefix       = "10.1.0.0/24"
}

resource "azurerm_public_ip" "public_ip_nginx" {
  name                         = "public_ip_nginx"
  resource_group_name          = "${azurerm_resource_group.rg-tf.name}"
  public_ip_address_allocation = "static"
  location                     = "${var.location}"
}

resource "azurerm_network_security_group" "nsg-nginx" {
  name                = "nsg-nginx"
  resource_group_name = "${azurerm_resource_group.rg-tf.name}"
  location            = "${var.location}"

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags {
    environment = "Terraform Demo"
  }
}

resource "azurerm_network_interface" "nic1_nginx" {
  name                = "nic1_nginx1"
  resource_group_name = "${azurerm_resource_group.rg-tf.name}"
  location            = "${var.location}"

  ip_configuration {
    name                          = "niccfg-nginx"
    subnet_id                     = "${azurerm_subnet.subnet-tf.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.public_ip_nginx.id}"
  }
}

resource "azurerm_virtual_machine" "nginx" {
  name                             = "nginx"
  resource_group_name              = "${azurerm_resource_group.rg-tf.name}"
  location                         = "${var.location}"
  network_interface_ids            = ["${azurerm_network_interface.nic1_nginx.id}"]
  vm_size                          = "Basic_A0"
  delete_os_disk_on_termination    = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.4"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk1"
    os_type           = "linux"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "nginx"
    admin_username = "${var.user}"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/${var.user}/.ssh/authorized_keys"
      key_data = "${file("~/.ssh/id_rsa.pub")}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum install epel-release -y",
      "sudo yum install nginx -y",
      "sudo systemctl start nginx",
    ]

    connection {
      type        = "ssh"
      host        = "${azurerm_public_ip.public_ip_nginx.ip_address}"
      user        = "${var.user}"
      private_key = "${file("~/.ssh/id_rsa")}"
    }
  }
}

###
# OUTPUT
###

output "azure_instance_public_ip" {
  value = "${azurerm_public_ip.public_ip_nginx.ip_address}"
}
