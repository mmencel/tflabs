# 10th Magnitude - Terraform Basics

Translating demo exercises in [PluralSight Terraform - Getting Started](https://app.pluralsight.com/library/courses/terraform-getting-started) to work in Azure.

## Customer

Matt

## Author

Matt Mencel <mmencel@10thmagnitude.com>

## Usage

A couple assumptions are made...

* Variables are stored in `TF_VAR` environment variables or in `global.tfvars`(copy `global.tfvars.example` to `global.tfvars` and edit as needed).
* Public/private key pair `id_rsa` exists.

1. cd to the exercise's directory
2. `terraform init`
3. `terraform plan -var-file=../global.tfvars`
4. `terraform apply -var-file=../global.tfvars`
5. Once completed: `terraform destroy -var-file=../global.tfvars`